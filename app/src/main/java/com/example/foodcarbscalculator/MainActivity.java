package com.example.foodcarbscalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button calculateButton;
    private EditText totalG;
    private EditText carbsG;
    private EditText foodG;
    private TextView resultG;
    private LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calculateButton = findViewById(R.id.calculateButton);
        totalG = findViewById(R.id.totalG);
        carbsG = findViewById(R.id.carbsG);
        foodG = findViewById(R.id.foodG);
        resultG = findViewById(R.id.resultG);
        mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // set result
                    int totG = Integer.parseInt(totalG.getText().toString());
                    int carG = Integer.parseInt(carbsG.getText().toString());
                    int fG = Integer.parseInt(foodG.getText().toString());
                    Integer res = (carG * fG )/totG;
                    resultG.setText( res.toString() );

                    //hide soft Keyboard
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch  (Exception e){
                    System.out.println("Text view not initialised");
                    Toast toast = Toast.makeText(MainActivity.this, "Give values for all fields", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 0);
                    toast.setMargin(0, 500);
                    toast.show();
                }
            }
        });
    }
}